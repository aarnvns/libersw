package com.felixion.libersw.model;

import com.felixion.libersw.database.model.Book;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;

import java.time.Year;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

public class BookTestUtils {

    private static Random random = new Random();

    public static Book createBook() {
        return initRandom(Book.builder()).build();
    }

    public static Book updateBook(Book book) {
        return initRandom(book.toBuilder())
                .id(book.getId())
                .build();
    }

    private static Book.BookBuilder initRandom(Book.BookBuilder bookBuilder) {
        int randomInt = random.nextInt();
        int yearOffsetFrom1900 = random.nextInt(118);

        return bookBuilder
                .author("author" + randomInt)
                .title("title" + randomInt)
                .publisher("publisher" + randomInt)
                .isbn("isbn" + randomInt)
                .category("category" + randomInt)
                .year(Year.of(1900 + yearOffsetFrom1900));
    }

    public static Matcher<Book> matchesBookIgnoringTimestamps(Book expected) {
        return allOf(
                hasProperty("id", is(expected.getId())),
                hasProperty("author", is(expected.getAuthor())),
                hasProperty("title", is(expected.getTitle())),
                hasProperty("publisher", is(expected.getPublisher())),
                hasProperty("isbn", is(expected.getIsbn())),
                hasProperty("category", is(expected.getCategory())),
                hasProperty("year", is(expected.getYear()))
        );
    }

    public static Matcher<Book> matchesBookIgnoringUpdatedTimestamp(Book expected) {
        return allOf(matchesBookIgnoringTimestamps(expected),
                hasProperty("createdTimestamp", is(expected.getCreatedTimestamp())));
    }

    public static Matcher<Book> matchesBookExactly(Book expected) {
        return allOf(matchesBookIgnoringUpdatedTimestamp(expected),
                hasProperty("updatedTimestamp", is(expected.getUpdatedTimestamp())));
    }

}
