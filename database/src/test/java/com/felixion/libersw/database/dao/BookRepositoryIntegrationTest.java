package com.felixion.libersw.database.dao;

import com.felixion.libersw.database.DatabaseConfiguration;
import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.model.BookTestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class BookRepositoryIntegrationTest {

    @Resource
    private BookRepository bookRepository;

    @Test
    public void createAndUpdate() {
        Book book = BookTestUtils.createBook();
        assertThat(book.getId(), nullValue());

        assertThat(bookRepository.save(book), BookTestUtils.matchesBookExactly(book));
        assertThat(book.getId(), notNullValue());

        Book lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookExactly(book));

        BookTestUtils.updateBook(book);
        assertThat(bookRepository.save(book), BookTestUtils.matchesBookIgnoringUpdatedTimestamp(book));

        lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookIgnoringUpdatedTimestamp(book));
    }

    @Test
    public void verifyTimestampsOnCreate() {
        Book book = BookTestUtils.createBook();
        assertThat(book, allOf(
                hasProperty("createdTimestamp", nullValue()),
                hasProperty("updatedTimestamp", nullValue())));

        Instant beforeCreationTimestamp = Instant.now();

        assertThat(bookRepository.save(book), BookTestUtils.matchesBookExactly(book));
        assertThat(book, allOf(
                hasProperty("createdTimestamp", notNullValue()),
                hasProperty("updatedTimestamp", notNullValue())));

        Instant afterCreationTimestamp = Instant.now();

        Book lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookExactly(book));
        assertThat(lookup.getCreatedTimestamp(),
                both(greaterThanOrEqualTo(beforeCreationTimestamp))
                        .and(lessThanOrEqualTo(afterCreationTimestamp)));
        assertThat(lookup.getUpdatedTimestamp(),
                both(greaterThanOrEqualTo(beforeCreationTimestamp))
                        .and(lessThanOrEqualTo(afterCreationTimestamp)));

    }

    @Test
    public void verifyTimestampsOnUpdate() {
        Book book = bookRepository.save(BookTestUtils.createBook());
        Book lookup = bookRepository.findById(book.getId()).orElseThrow();

        Instant beforeUpdateTimestamp = Instant.now();
        book = bookRepository.save(BookTestUtils.updateBook(book));
        Instant afterUpdateTimestamp = Instant.now();

        lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookExactly(book));
        assertThat(lookup.getCreatedTimestamp(), lessThanOrEqualTo(beforeUpdateTimestamp));
        assertThat(lookup.getUpdatedTimestamp(),
                both(greaterThanOrEqualTo(beforeUpdateTimestamp))
                        .and(lessThanOrEqualTo(afterUpdateTimestamp)));
    }

    @Test
    public void cannotModifyTimestamps() {
        Instant now = Instant.now().truncatedTo(ChronoUnit.MILLIS);
        Book book = bookRepository.save(BookTestUtils.createBook());

        book = BookTestUtils.updateBook(book).toBuilder()
                .createdTimestamp(now.minus(1, ChronoUnit.HOURS))
                .updatedTimestamp(now.minus(1, ChronoUnit.HOURS))
                .build();

        bookRepository.save(book);

        Book lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookIgnoringTimestamps(book));
        // just proving that the database will let us alter create timestamp (consider defensive measures)
        assertThat(lookup.getCreatedTimestamp(), lessThanOrEqualTo(now));
        assertThat(lookup.getUpdatedTimestamp(), greaterThanOrEqualTo(now));
    }

    @Test
    public void cannotInitializeTimestamps() {
        Instant now = Instant.now();

        Book book = BookTestUtils.createBook().toBuilder()
                .createdTimestamp(now.minus(1, ChronoUnit.HOURS))
                .updatedTimestamp(now.minus(1, ChronoUnit.HOURS))
                .build();

        Book savedBook = bookRepository.save(book);
        assertThat(bookRepository.save(book), BookTestUtils.matchesBookExactly(book));
        assertThat(savedBook.getCreatedTimestamp(), greaterThanOrEqualTo(now));
        assertThat(savedBook.getUpdatedTimestamp(), greaterThanOrEqualTo(now));

        Book lookup = bookRepository.findById(book.getId()).orElseThrow();
        assertThat(lookup, BookTestUtils.matchesBookExactly(savedBook));
        assertThat(lookup.getCreatedTimestamp(), greaterThanOrEqualTo(now));
        assertThat(lookup.getUpdatedTimestamp(), greaterThanOrEqualTo(now));
    }

}
