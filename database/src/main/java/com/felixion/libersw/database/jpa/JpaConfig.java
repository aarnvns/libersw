package com.felixion.libersw.database.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "org.springframework.data.domain.AuditorAware-null")
public class JpaConfig {
}
