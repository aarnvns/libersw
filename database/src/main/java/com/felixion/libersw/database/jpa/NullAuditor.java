package com.felixion.libersw.database.jpa;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("org.springframework.data.domain.AuditorAware-null")
public class NullAuditor implements AuditorAware {

    @Override
    public Optional getCurrentAuditor() {
        return Optional.empty();
    }

}
