package com.felixion.libersw.database.dao;

import javax.persistence.AttributeConverter;
import java.time.Year;
import java.util.Optional;


public class YearConverter implements AttributeConverter<Year, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Year attribute) {
        return Optional.ofNullable(attribute)
                .map(Year::getValue)
                .orElse(null);
    }

    @Override
    public Year convertToEntityAttribute(Integer dbData) {
        return Optional.ofNullable(dbData)
                .map(Year::of)
                .orElse(null);
    }

}
