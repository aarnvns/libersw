package com.felixion.libersw.database;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.felixion.libersw.database")
@EnableAutoConfiguration
public class DatabaseConfiguration {
}
