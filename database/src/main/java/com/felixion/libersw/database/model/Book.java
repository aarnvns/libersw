package com.felixion.libersw.database.model;

import com.felixion.libersw.database.dao.YearConverter;

import javax.persistence.*;
import java.time.Instant;
import java.time.Year;
import java.util.UUID;

@Entity
public class Book extends AbstractEntity {

    private String author;

    private String title;

    private String category;

    private String isbn;

    private String publisher;

    @Convert(converter = YearConverter.class)
    private Year year;

    public Book() {
    }

    public Book(Book copy) {
        super(copy);
        author = copy.author;
        title = copy.title;
        category = copy.category;
        isbn = copy.isbn;
        publisher = copy.publisher;
        year = copy.year;
    }

    public static BookBuilder builder() {
        return new BookBuilder();
    }

    public BookBuilder toBuilder() {
        return new BookBuilder(this);
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public static final class BookBuilder {
        private Book book;

        private BookBuilder() {
            this.book = new Book();
        }

        private BookBuilder(Book book) {
            this.book = new Book(book);
        }

        public BookBuilder author(String author) {
            book.setAuthor(author);
            return this;
        }

        public BookBuilder title(String title) {
            book.setTitle(title);
            return this;
        }

        public BookBuilder category(String category) {
            book.setCategory(category);
            return this;
        }

        public BookBuilder isbn(String isbn) {
            book.setIsbn(isbn);
            return this;
        }

        public BookBuilder publisher(String publisher) {
            book.setPublisher(publisher);
            return this;
        }

        public BookBuilder year(Year year) {
            book.setYear(year);
            return this;
        }

        public BookBuilder id(UUID id) {
            book.setId(id);
            return this;
        }

        public BookBuilder createdTimestamp(Instant createdTimestamp) {
            book.setCreatedTimestamp(createdTimestamp);
            return this;
        }

        public BookBuilder updatedTimestamp(Instant updatedTimestamp) {
            book.setUpdatedTimestamp(updatedTimestamp);
            return this;
        }

        public Book build() {
            return book;
        }
    }
}
