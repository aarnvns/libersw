package com.felixion.libersw.database.dao;

import com.felixion.libersw.database.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface BookRepository extends CrudRepository<Book, UUID> {
}
