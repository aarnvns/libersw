package com.felixion.libersw.database.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    /*
     * Database timestamps will use Calendar under the covers, and expose these timestamps
     * to the code as Instants.
     *
     * This is because JPA only supports java.util.Date or java.util.Calendar with @Temporal,
     * and java.util.Date objects loaded from the database are really java.sql.Timestamp objects
     * (having nanoseconds) but that isn't compatible with the java.util.Date objects the persistence
     * layer puts into the object in-memory when persisting it
     */

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdTimestamp;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar updatedTimestamp;

    protected AbstractEntity() {
    }

    protected AbstractEntity(AbstractEntity copy) {
        id = copy.id;
        createdTimestamp = copy.createdTimestamp;
        updatedTimestamp = copy.updatedTimestamp;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public Instant getCreatedTimestamp() {
        if (createdTimestamp == null) {
            return null;
        }
        return createdTimestamp.toInstant();
    }

    public void setCreatedTimestamp(Instant createdTimestamp) {
        if (createdTimestamp == null) {
            this.createdTimestamp = null;
            return;
        }
        this.createdTimestamp = GregorianCalendar.from(createdTimestamp.atZone(ZoneId.systemDefault()));
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    public Instant getUpdatedTimestamp() {
        if (updatedTimestamp == null) {
            return null;
        }
        return updatedTimestamp.toInstant();
    }

    public void setUpdatedTimestamp(Instant updatedTimestamp) {
        if (updatedTimestamp == null) {
            this.updatedTimestamp = null;
            return;
        }
        this.updatedTimestamp = GregorianCalendar.from(updatedTimestamp.atZone(ZoneId.systemDefault()));
    }

}
