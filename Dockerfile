FROM openjdk:10-jre
ADD server/target/libersw-app.jar server.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/server.jar"]
