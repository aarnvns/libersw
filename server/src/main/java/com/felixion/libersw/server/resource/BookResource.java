package com.felixion.libersw.server.resource;

import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.server.resource.util.BadRequestPreconditions;
import com.felixion.libersw.server.resource.param.UuidQueryParam;
import com.felixion.libersw.server.service.BookService;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/v1/book")
public class BookResource {

    private BookService bookService;

    @RequestMapping(method = RequestMethod.POST)
    public Book createBook(@RequestBody @NotNull Book book) {
        BadRequestPreconditions.checkCondition(book.getId() == null, "Cannot create book with ID");
        return bookService.create(book);
    }

    @RequestMapping(path = "{id}", method = RequestMethod.PUT)
    public Book updateBook(@PathVariable("id") @NotNull UuidQueryParam id, @RequestBody @NotNull Book book) {
        BadRequestPreconditions.checkCondition(book.getId() == null || book.getId().equals(id.getId()), "Cannot update book containing a different ID");
        book.setId(id.getId());
        return bookService.update(book);
    }

    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Book> getBook(@PathVariable("id") UuidQueryParam id) {
        return bookService.find(id.getId())
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Resource
    @VisibleForTesting
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

}
