package com.felixion.libersw.server.resource.util;

/**
 * Used for "IllegalArgumentExceptions" that should result in 400 return codes
 */
public class BadRequestException extends RuntimeException {

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

}
