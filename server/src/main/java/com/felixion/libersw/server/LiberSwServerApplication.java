package com.felixion.libersw.server;

import com.felixion.libersw.database.DatabaseConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(DatabaseConfiguration.class)
public class LiberSwServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiberSwServerApplication.class, args);
    }

}
