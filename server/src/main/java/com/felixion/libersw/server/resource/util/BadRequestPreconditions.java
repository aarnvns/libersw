package com.felixion.libersw.server.resource.util;

/**
 * Simple utility for generating BadRequestExceptions based on conditions
 */
public class BadRequestPreconditions {

    private BadRequestPreconditions() {
    }

    public static void checkCondition(boolean condition, String message) {
        if (!condition) {
            throw new BadRequestException(message);
        }
    }

}
