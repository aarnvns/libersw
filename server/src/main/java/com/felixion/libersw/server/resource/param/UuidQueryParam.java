package com.felixion.libersw.server.resource.param;

import org.springframework.core.convert.converter.Converter;

import java.util.UUID;

public class UuidQueryParam {

    private final UUID id;

    private UuidQueryParam(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public static UuidQueryParam newUuidQueryParam(UUID id) {
        return new UuidQueryParam(id);
    }

    public static class LongIdConverter implements Converter<String, UuidQueryParam> {
        @Override
        public UuidQueryParam convert(String idStr) {
            UUID uuid = UUID.fromString(idStr);
            return new UuidQueryParam(uuid);
        }
    }

}
