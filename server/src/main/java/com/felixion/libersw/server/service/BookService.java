package com.felixion.libersw.server.service;

import com.felixion.libersw.database.dao.BookRepository;
import com.felixion.libersw.database.model.Book;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class BookService {

    @Resource
    private BookRepository bookRepository;

    @Transactional
    public Book create(Book book) {
        Preconditions.checkArgument(book.getId() == null, "Cannot create a book with a specific ID");
        return bookRepository.save(book);
    }

    @Transactional
    public Book update(Book book) {
        Preconditions.checkArgument(book.getId() != null, "Cannot update book without ID");
        return bookRepository.save(book);
    }

    @Transactional
    public Optional<Book> find(UUID bookId) {
        return bookRepository.findById(bookId);
    }

    @VisibleForTesting
    protected void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

}
