package com.felixion.libersw.server.service;

import com.felixion.libersw.database.dao.BookRepository;
import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.model.BookTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    BookService bookService;

    @Mock
    BookRepository bookRepository;

    @Before
    public void setUp() {
        bookService = new BookService();
        bookService.setBookRepository(bookRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createBookHasId() {
        Book book = BookTestUtils.createBook().toBuilder()
                .id(UUID.randomUUID())
                .build();
        bookService.create(book);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateBookMissingId() {
        Book book = BookTestUtils.createBook();
        bookService.update(book);
    }

}
