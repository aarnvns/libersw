package com.felixion.libersw.server.resource;

import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.model.BookTestUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookResourceIntegrationTest {

    @LocalServerPort
    private int port;

    @Resource
    private TestRestTemplate restTemplate;

    @Test
    public void createBookUpdateAndFind() {
        Book book = BookTestUtils.createBook();

        ResponseEntity<Book> responseEntity = restTemplate.postForEntity(createUrlFromRelative("/v1/book"), book, Book.class);
        assertThat(responseEntity, successfulBookResponse());

        Book response = responseEntity.getBody();
        book.setId(response.getId());
        assertThat(response, BookTestUtils.matchesBookIgnoringTimestamps(book));

        book = BookTestUtils.updateBook(response);
        responseEntity = restTemplate.exchange(createUrlFromRelative("/v1/book/" + book.getId()),
                HttpMethod.PUT, new HttpEntity<>(book, new HttpHeaders()), Book.class);
        assertThat(responseEntity, successfulBookResponse());

        response = responseEntity.getBody();
        assertThat(response, BookTestUtils.matchesBookIgnoringTimestamps(book));

        responseEntity = restTemplate.getForEntity(createUrlFromRelative("/v1/book/" + book.getId()), Book.class);
        assertThat(responseEntity, successfulBookResponse());
        Book lookup = responseEntity.getBody();
        assertThat(lookup, BookTestUtils.matchesBookIgnoringTimestamps(book));
    }

    private String createUrlFromRelative(String uriPath) {
        return "http://localhost:" + port + uriPath;
    }

    private Matcher<ResponseEntity<Book>> successfulBookResponse() {
        return allOf(
                hasProperty("statusCode", is(HttpStatus.OK)),
                hasProperty("body"), notNullValue(),
                hasProperty("body",
                        hasProperty("id", notNullValue())));
    }

}
