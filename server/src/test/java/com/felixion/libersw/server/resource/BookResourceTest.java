package com.felixion.libersw.server.resource;

import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.model.BookTestUtils;
import com.felixion.libersw.server.resource.param.UuidQueryParam;
import com.felixion.libersw.server.resource.util.BadRequestException;
import com.felixion.libersw.server.service.BookService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookResourceTest {

    @Mock
    private BookService bookService;

    private BookResource bookResource;

    @Before
    public void init() {
        bookResource = new BookResource();
        bookResource.setBookService(bookService);
    }

    @Test(expected = BadRequestException.class)
    public void createBookWithId() {
        Book book = BookTestUtils.createBook().toBuilder()
                .id(UUID.randomUUID())
                .build();
        bookResource.createBook(book);
    }

    @Test(expected = BadRequestException.class)
    public void updateBookWrongId() {
        Book book = BookTestUtils.createBook().toBuilder()
                .id(UUID.randomUUID())
                .build();

        bookResource.updateBook(UuidQueryParam.newUuidQueryParam(UUID.randomUUID()), book);
    }

    @Test
    public void getBookIdNotFound() {
        when(bookService.find(any(UUID.class))).thenReturn(Optional.empty());

        ResponseEntity<Book> responseEntity = bookResource.getBook(UuidQueryParam.newUuidQueryParam(UUID.randomUUID()));
        assertThat(responseEntity.getStatusCode(), Matchers.is(HttpStatus.NOT_FOUND));
    }

}
