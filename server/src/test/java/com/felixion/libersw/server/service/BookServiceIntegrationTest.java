package com.felixion.libersw.server.service;

import com.felixion.libersw.database.model.AbstractEntity;
import com.felixion.libersw.database.model.Book;
import com.felixion.libersw.model.BookTestUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class BookServiceIntegrationTest {

    @Resource
    private BookService bookService;

    @Test
    public void createFindAndUpdateBook() {
        Book book = bookService.create(BookTestUtils.createBook());
        Book lookup = bookService.find(book.getId()).orElseThrow();

        assertThat(lookup, samePropertyValuesAs(book));

        // update book
        book = bookService.update(BookTestUtils.updateBook(book));
        lookup = bookService.find(book.getId()).orElseThrow();

        assertThat(lookup, samePropertyValuesAs(book));
    }

}
